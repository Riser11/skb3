import express from 'express';
import cors from 'cors';

const fetch = require('isomorphic-fetch');
const app = express();

app.use(cors());

const jsonFile = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';
app.computer={}

  app.prepare = async() => {
		let res;
		try{
			res = await fetch(jsonFile)

			if(!res)
				throw new Error("Не удалось загрузить структуру компьютера");

			app.computer = await res.json();

		}
		catch(err) {
			console.log(err);
		}
	}

app.prepare()


  app.all('*', (req, res) => {
  let text = req.originalUrl.match( /[a-z]*/ig )
 for(let i=0; i<text.length;i++){
   if(text[i]==''){
     text.splice(i,1)

   }
 }
 for(let i=0; i<text.length;i++){
   if(text[i]==''){
     text.splice(i,1)

   }
 }

 for(let i=0; i<text.length;i++){
   if(text[i]==''){
     text.splice(i,1)

   }
 }


 if(text.length==0){
     return res.json(app.computer)
 }
  if(text.length==1){
    return res.json(app.computer[text[0]]);
  }

  if(text.length==2){
    return res.json(app.computer[text[0]][text[1]]);
  }
  if(text.length==3){
    return res.json(app.computer[text[0]][text[1]][text[2]]);
  }
  })


app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
